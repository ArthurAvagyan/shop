# Date : 08/12/2018 23:46
# Author : arthur


class WorkersView:
    @classmethod
    def menu(cls):
        print("\n1. Add worker",
              "\n2. Edit worker",
              "\n3. Remove worker",
              "\n4. Back")

    @classmethod
    def printWorkers(cls, data):
        print("{:<3s}{:<15s}{:<10s}".format("ID", "Name", "Salary"))
        print("_" * 28)

        for item in data:
            print("{:<3d}{:<15s}{:<10.2f}".format(item["id"], item["name"], item["salary"]))


class WorkersModel:
    @staticmethod
    def existsID(data, exID):
        for index, item in enumerate(data):
            if item["id"] == exID:
                return True
        return False

    @staticmethod
    def generateID(data):
        newID = 0
        for element in data:
            if element["id"] > newID:
                newID = element["id"]
        return newID + 1

    @classmethod
    def addWorker(cls, data, addName, addSalary):
        addID = cls.generateID(data)

        addWorker = {
            "id": addID,
            "name": addName,
            "salary": addSalary
        }

        data.append(addWorker)

        return data

    @classmethod
    def editWorker(cls, data, editID, editName, editSalary):
        for index, item in enumerate(data):
            if item["id"] == editID:
                editItem = {
                    "id": editID,
                    "name": editName,
                    "salary": editSalary
                }

                data[index] = editItem

                return data

    @classmethod
    def removeWorker(cls, data, removeID):
        for index, element in enumerate(data):
            if element["id"] == removeID:
                data.pop(index)
                return data
