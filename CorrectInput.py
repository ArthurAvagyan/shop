# Date : 08/12/2018 23:46
# Author : arthur


def isInputInt(inputNum):
    # Returning True if input is integer, False in other way
    if inputNum.isdigit():
        return True
    return False


def isInputFloat(inputNum):
    # Returning True if input is float or integer, False in other way
    if inputNum.replace(".", "", 1).isdigit():
        return True
    return False


def inputInt(inputText):
    # Getting safe input
    inputNum = input(inputText)

    while not isInputInt(inputNum):
        inputNum = input("Wrong input.\n" + inputText)
    return int(inputNum)


def inputFloat(inputText):
    # Getting safe input
    inputNum = input(inputText)

    while not isInputFloat(inputNum):
        inputNum = input("Wrong input\n" + inputText)
    return float(inputNum)
