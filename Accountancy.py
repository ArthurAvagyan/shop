# Date : 09/12/2018 22:31
# Author : arthur


class AccountancyView:
    @classmethod
    def printSoldData(cls, soldData):
        print("\nSold:\n{:<10s}{:<15s}{:<10s}{:<10s}".format("Item ID", "Name", "Price", "Quantity"))
        print("_" * 45, end="")
        print()
        for item in soldData:
            print("{:<10d}{:<15s}{:<10.2f}{:<10d}".format(item["id"], item["name"], item["price"], item["quantity"]))


class AccountancyModel:
    totalSales = []

    @classmethod
    def buy(cls, data, buyID, quantity):
        for item in data:
            if item["id"] == buyID:
                for soldItem in cls.totalSales:
                    if item["id"] == soldItem["id"]:
                        soldItem["quantity"] += quantity
                        return

                sold = {
                    "id": item["id"],
                    "name": item["name"],
                    "price": item["price"],
                    "quantity": quantity
                }

                cls.totalSales.append(sold)

    @classmethod
    def getSoldData(cls):
        return cls.totalSales
