# Date : 08/12/2018 23:46
# Author : arthur


import json
from UserView import UserView
import CorrectInput
from Admin import AdminView, AdminModel
from Shop import ShopView, ShopModel
from Workers import WorkersView, WorkersModel
from Accountancy import AccountancyView, AccountancyModel
from BuyerView import BuyerView


def start():
    # Printing user menu and getting user option

    UserView.menu()
    userInput = CorrectInput.inputInt("Choose what you want to do: ")

    while userInput != 3:
        if userInput == 1:
            # Printing buyer menu and getting user option
            BuyerView.menu()
            buyerInput = CorrectInput.inputInt("Choose what you want to do: ")

            while buyerInput != 3:
                if buyerInput == 1:
                    with open('ShopItems.json') as shopData:
                        data = json.loads(shopData.read())

                    # Printing items ang getting ID of item buyer wants to buy
                    ShopView.printItems(data)
                    buyID = CorrectInput.inputInt("\nEnter item ID You want to buy. (Enter 0 to exit): ")

                    while buyID != 0:
                        if ShopModel.existsID(data, buyID):
                            # Getting quantity and comparing with existing quantity
                            buyQuantity = CorrectInput.inputInt("Enter quantity You want to buy: ")
                            availQuantity = ShopModel.availQuant(data, buyID, buyQuantity)

                            while buyQuantity > availQuantity:
                                print("We have only {}.".format(availQuantity))
                                buyQuantity = CorrectInput.inputInt("Enter quantity You want to buy: ")

                            # Subtracting quantity from from data and adding item information to "Shopping cart"
                            AccountancyModel.buy(data, buyID, buyQuantity)
                            data = ShopModel.buy(data, buyID, buyQuantity)

                            with open('ShopItems.json', 'w') as shopData:
                                json.dump(data, shopData)
                        else:
                            print("Item with {} ID doesn't exist.".format(buyID))

                        # Getting ID of item buyer wants to buy
                        buyID = CorrectInput.inputInt("\nEnter item ID You want to buy. (Enter 0 to exit): ")
                elif buyerInput == 2:
                    # printing Shopping cart
                    boughtItems = AccountancyModel.totalSales
                    AccountancyView.printSoldData(boughtItems)
                else:
                    print("Wrong option")

                # Printing buyer menu and getting user option
                BuyerView.menu()
                buyerInput = CorrectInput.inputInt("Choose what you want to do: ")

        elif userInput == 2:
            # Getting admin password from user and comparing with admin password
            userPassword = None
            while userPassword != AdminModel.password:
                userPassword = input("Please enter the password. (Enter 0 to go back): ")
                if userPassword == "0":
                    break
            if userPassword == AdminModel.password:
                # Printing admin menu and getting admin option
                AdminView.menu()
                adminInput = CorrectInput.inputInt("Choose what you want to do: ")

                while adminInput != 4:
                    if adminInput == 1:
                        # Printing shop menu and getting admin option
                        ShopView.menu()
                        adminInput = CorrectInput.inputInt("Choose what you want to do: ")

                        while adminInput != 4:
                            with open('ShopItems.json') as shopData:
                                data = json.loads(shopData.read())

                            if adminInput == 1:
                                # Printing shop items and getting new item's name, price, quantity
                                ShopView.printItems(data)

                                addName = input("Enter item name: ")
                                addPrice = CorrectInput.inputFloat("Enter item price: ")
                                addQuantity = CorrectInput.inputInt("Enter item quantity: ")

                                data = ShopModel.addItem(data, addName, addPrice, addQuantity)

                                ShopView.printItems(data)

                            elif adminInput == 2:
                                # Printing shop items and getting ID of item to edit and checking for validity
                                ShopView.printItems(data)

                                editID = CorrectInput.inputInt("Enter item ID: ")

                                if ShopModel.existsID(data, editID):
                                    # Entering name, price, quantity ot edit
                                    editName = input("New name: ")
                                    editPrice = CorrectInput.inputFloat("New price: ")
                                    editQuantity = CorrectInput.inputInt("New quantity: ")

                                    data = ShopModel.editItem(data, editID, editName, editPrice, editQuantity)

                                    ShopView.printItems(data)
                                else:
                                    print("Item with {} ID doesn't exist.".format(editID))

                            elif adminInput == 3:
                                # Printing shop items and getting ID of item to remove and checking for validity
                                ShopView.printItems(data)

                                removeID = CorrectInput.inputInt("Enter item ID You want to remove: ")

                                if ShopModel.existsID(data, removeID):
                                    ShopModel.removeItem(data, removeID)

                                    ShopView.printItems(data)
                                else:
                                    print("Worker with {} ID doesn't exist.".format(removeID))

                            else:
                                print("Wrong option")

                            with open('ShopItems.json', 'w') as shopData:
                                json.dump(data, shopData)

                            ShopView.menu()
                            adminInput = CorrectInput.inputInt("Choose what you want to do: ")
                    elif adminInput == 2:
                        # Printing worker menu and getting admin option
                        WorkersView.menu()
                        adminInput = CorrectInput.inputInt("Choose what you want to do: ")

                        while adminInput != 4:
                            with open('Workers.json') as workerData:
                                data = json.loads(workerData.read())

                            if adminInput == 1:
                                # Printing workers and getting new worker's name, salary
                                WorkersView.printWorkers(data)

                                addName = input("Enter worker name: ")
                                addSalary = CorrectInput.inputFloat("Enter worker salary: ")

                                data = WorkersModel.addWorker(data, addName, addSalary)

                                WorkersView.printWorkers(data)

                            elif adminInput == 2:
                                # Printing workers and getting worker's ID  and checking for validity
                                WorkersView.printWorkers(data)

                                editID = CorrectInput.inputInt("Enter item ID: ")

                                if WorkersModel.existsID(data, editID):
                                    # Getting name and salary of worker to edit
                                    editName = input("New name: ")
                                    editSalary = CorrectInput.inputFloat("New salary: ")

                                    data = WorkersModel.editWorker(data, editID, editName, editSalary)

                                    WorkersView.printWorkers(data)
                                else:
                                    print("Item with {} ID doesn't exist.".format(editID))

                            elif adminInput == 3:
                                # Printing workers and getting worker's ID to remove and checking for validity
                                WorkersView.printWorkers(data)

                                removeID = CorrectInput.inputInt("Enter item ID You want to remove: ")

                                if WorkersModel.existsID(data, removeID):
                                    WorkersModel.removeWorker(data, removeID)

                                    WorkersView.printWorkers(data)
                                else:
                                    print("Worker with {} ID doesn't exist.".format(removeID))
                            else:
                                print("Wrong option")

                            with open('Workers.json', 'w') as workerData:
                                json.dump(data, workerData)

                            WorkersView.menu()
                            adminInput = CorrectInput.inputInt("Choose what you want to do: ")
                    elif adminInput == 3:
                        # Printing workers' data and shop items' data
                        with open('Workers.json') as workerData:
                            data = json.loads(workerData.read())

                        WorkersView.printWorkers(data)
                        soldData = AccountancyModel.getSoldData()
                        AccountancyView.printSoldData(soldData)
                    else:
                        print("Wrong option")

                    AdminView.menu()
                    adminInput = CorrectInput.inputInt("Choose what you want to do: ")
        else:
            print("Wrong option")

        UserView.menu()
        userInput = CorrectInput.inputInt("Choose what you want to do: ")


start()
