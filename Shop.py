# Date : 08/12/2018 23:46
# Author : arthur


class ShopView:
    @classmethod
    def menu(cls):
        print("\n1. Add items",
              "\n2. Edit items",
              "\n3. Remove Items",
              "\n4. Back")

    @classmethod
    def printItems(cls, data):
        print("{:<10s}{:<15s}{:<10s}{:<10s}".format("Item ID", "Name", "Price", "Quantity"))
        print("_" * 45)

        for item in data:
            print("{:<10d}{:<15s}{:<10.2f}{:<10d}".format(item["id"], item["name"], item["price"], item["quantity"]))


class ShopModel:
    @staticmethod
    def existsID(data, exID):
        for index, item in enumerate(data):
            if item["id"] == exID:
                return True
        return False

    @staticmethod
    def generateID(data):
        newID = 0
        for element in data:
            if element["id"] > newID:
                newID = element["id"]
        return newID + 1

    @classmethod
    def buy(cls, data, buyID, buyQuantity):
        for item in data:
            if item["id"] == buyID:
                item["quantity"] -= buyQuantity
                return data

    @classmethod
    def addItem(cls, data, addName, addPrice, addQuantity):
        addID = cls.generateID(data)

        addItem = {
            "id": addID,
            "name": addName,
            "price": addPrice,
            "quantity": addQuantity
        }

        data.append(addItem)

        return data

    @classmethod
    def editItem(cls, data, editID, editName, editPrice, editQuantity):
        for index, item in enumerate(data):
            if item["id"] == editID:
                editItem = {
                    "id": editID,
                    "name": editName,
                    "price": editPrice,
                    "quantity": editQuantity
                }

                data[index] = editItem

                return data

    @classmethod
    def removeItem(cls, data, removeID):
        for index, element in enumerate(data):
            if element["id"] == removeID:
                data.pop(index)
                return data

    @classmethod
    def availQuant(cls, data, buyID, buyQuant):
        for item in data:
            if item["id"] == buyID:
                return item["quantity"]
